import React, { Component } from 'react';
import { AppState, StatusBar } from 'react-native';
import { Provider } from 'react-redux';

import '../I18n/I18n'; // import this before RootContainer as RootContainer is using react-native-i18n, and I18n.js needs to be initialized before that!
import RootContainer from './RootContainer';
import AppConfig from '../Config/AppConfig';
import StartupRedux from '../Redux/StartupRedux';
import createStore from '../Redux';
import { Fonts, Metrics } from '../Themes';

import Log from '../Utils/Log';
const log = new Log('Containers/App');

const { config } = AppConfig;

let store = createStore();

export const getState = () => {

  if (store) {
    return store.getState();
  } else {
    log.warn('Tried to access state before it was initialized!');
    return null;
  }
};

// Track user activity
if (!__DEV__) {
  log.enableUserTracking();
}
log.action('App', 'Startup', 'Timestamp', new Date());
log.action('GUI', 'AppInForeground', true);

class App extends Component {
  constructor() {
    super();
    this.appStateHandler = this.appStateHandler.bind(this);
  }

  componentDidMount() {
    // Register AppState-Handler
    AppState.addEventListener('change', this.appStateHandler);
    // actively set statusbar to translucent to prevent some display errors on android
    if (Metrics.androidStatusBarTranslucent) {
      StatusBar.setTranslucent(true);
    }
  }

  render() {
    return (
      <Provider store={store}>
        <RootContainer />
      </Provider>
    );
  }

  appStateHandler(newAppState) {
    store.dispatch(StartupRedux.appStateChange(newAppState));
  }
}

export default App;
