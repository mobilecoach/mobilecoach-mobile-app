import React, { Component } from 'react';
import { StatusBar, SafeAreaView } from 'react-native';
import ReduxNavigation from '../Navigation/ReduxNavigation';
import { connect } from 'react-redux';
import StartupActions from '../Redux/StartupRedux';
import ServerSyncActions from '../Redux/ServerSyncRedux';
import Brand from '../Themes/Brand';
import { Colors, Metrics } from '../Themes/';
class RootContainer extends Component {

  UNSAFE_componentWillReceiveProps(newProps) {
    const oldScreen = this.props ? this.props.currentScreen : null;
    const newScreen = newProps.guistate
      ? newProps.guistate.currentScreen
      : null;
  }

  render() {
    const {hydrationCompleted} = this.props;

    return (
      <>
        {hydrationCompleted ? (
          <SafeAreaView
            style={{flex: 1, backgroundColor: Brand.colors.primary}}>
            <StatusBar
              backgroundColor={Brand.colors.primary}
              barStyle="light-content"
            />
            <ReduxNavigation />
          </SafeAreaView>
        ) : null}
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  hydrationCompleted: state.hydrationCompleted.hydrationCompleted,
  currentScreen: state.guistate.currentScreen,
});

const mapDispatchToProps = (dispatch) => ({
  startup: () => dispatch(StartupActions.startup()),
  serverSyncInitialize: () => dispatch(ServerSyncActions.initialize()),
});

export default connect(mapStateToProps, mapDispatchToProps)(RootContainer);
