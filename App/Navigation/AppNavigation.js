import React from 'react';

import {NavigationContainer, DrawerActions} from '@react-navigation/native';
import {createDrawerNavigator, DrawerItemList} from '@react-navigation/drawer';
import {createStackNavigator} from '@react-navigation/stack';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Alert
} from 'react-native';
import {Badge} from 'react-native-elements';
import {Colors} from '../Themes';
import IconEntypo from 'react-native-vector-icons/Entypo';
import IconIonicon from 'react-native-vector-icons/Ionicons';
import { normalize } from '../Utils/Common';
import ConnectionStateButton from '../Components/ConnectionStateButton';
import { ConnectionStates } from '../Redux/ServerSyncRedux';
import LoadingContainer from '../Containers/LoadingContainer';

import Chat from '../Containers/Chat/Chat';

import ScreenStartWithLogo from '../Containers/Onboarding/ScreenStartWithLogo';
import ScreenLanguageSelection from '../Containers/Onboarding/ScreenLanguageSelection';
import ScreenCoachSelection from '../Containers/Onboarding/ScreenCoachSelection';
import ScreenWelcomeByCoach from '../Containers/Onboarding/ScreenWelcomeByCoach';

import I18n from '../I18n/I18n';
import {connect} from 'react-redux';

const StackOnboarding = createStackNavigator();

export const initialRouteName = 'ScreenStartWithLogo';
function Onboarding() {
  return (
    <StackOnboarding.Navigator
      initialRouteName
      screenOptions={{
        headerShown: false,
        gestureEnabled: false,
      }}>
      <StackOnboarding.Screen
        name="ScreenStartWithLogo"
        component={ScreenStartWithLogo}
      />
      <StackOnboarding.Screen
        name="ScreenLanguageSelection"
        component={ScreenLanguageSelection}
      />
      <StackOnboarding.Screen
        name="ScreenCoachSelection"
        component={ScreenCoachSelection}
      />
      <StackOnboarding.Screen
        name="ScreenWelcomeByCoach"
        component={ScreenWelcomeByCoach}
      />
    </StackOnboarding.Navigator>
  );
}

const DrawerSideMenu = createDrawerNavigator();
function SideMenu({
  route,
  navigation,
  unreadMessages,
  connectionState,
  coach,
}) {
  const {screenProps} = route.params;
  const renderHamburger = props => {
    return (
      <View>
        <TouchableOpacity
          onPress={() => navigation.dispatch(DrawerActions.openDrawer())}>
          <IconEntypo name="menu" color="white" size={32} />
        </TouchableOpacity>
      </View>
    );
  };

  const showConnectionStateMessage = (connectionState) => {
    let alertMessage = null;
    switch (connectionState) {
      case ConnectionStates.INITIALIZING:
      case ConnectionStates.INITIALIZED:
        alertMessage = I18n.t('ConnectionStates.initialized');
        break;
      case ConnectionStates.CONNECTING:
      case ConnectionStates.RECONNECTING:
        alertMessage = I18n.t('ConnectionStates.connecting');
        break;
      case ConnectionStates.CONNECTED:
      case ConnectionStates.SYNCHRONIZATION:
        alertMessage = I18n.t('ConnectionStates.connected');
        break;
      case ConnectionStates.SYNCHRONIZED:
        alertMessage = I18n.t('ConnectionStates.synchronized');
        break;
    }

    Alert.alert(
      I18n.t('ConnectionStates.connectionToCoach'),
      alertMessage,
      [{ text: I18n.t('Common.ok'), onPress: () => true }],
      { cancelable: false },
    );
  };


  const renderConnectionState = props => {
    return (
    <View>
      <ConnectionStateButton
      onPress={() => {
        showConnectionStateMessage(connectionState);
      }}
      connectionState={connectionState}
    />
    </View>
    );
  }

  const CustomDrawerContent = props => {

    return (
      <View
        style={{flex: 1, flexDirection: 'column', backgroundColor: '#FCF7F0'}}>
        <View style={{flex: 1}}>
          <DrawerItemList {...props} />
        </View>
      </View>
    );
  };

  return (
    <DrawerSideMenu.Navigator
      initialRouteName={'Chat'}
      drawerContent={props => {
        return <CustomDrawerContent {...props} />;
      }}
      screenOptions={{
        headerShown: true,
        headerStyle: styles.navbar,
        headerTintColor: 'white',
        headerTitleAlign: 'center',
        headerTitleStyle: {
          fontSize: normalize(16),
          paddingTop: 5,
        },
        headerLeft: renderHamburger,
        headerRight: renderConnectionState,
        swipeEnabled: false,
        drawerType: 'front',
      }}>
      <DrawerSideMenu.Screen
        name="Chat"
        component={Chat}
        initialParams={{screenProps}}
        options={{
          title: I18n.t('Menu.Chat', {coach: I18n.t(`Coaches.${coach}`)}),
          drawerIcon: ({focused}) => (
            <IconEntypo name="chat" style={styles.actionButtonIcon} />
          ),
          drawerLabel: () => (
            <View style={styles.drawerLabel}>
              <Text style={styles.drawerLabelText}>{I18n.t('Menu.Chat', {coach: I18n.t(`Coaches.${coach}`)})}</Text>
              <Badge
                badgeStyle={styles.badge}
                textStyle={styles.badgeText}
                value={unreadMessages}
              />
            </View>
          ),
        }}
      />
    </DrawerSideMenu.Navigator>
  );
}

const StackNavigatorRoot = createStackNavigator();
function RootStack({screenProps}) {
  return (
    <StackNavigatorRoot.Navigator
      initialRouteName={LoadingContainer}
      screenOptions={{
        gestureEnabled: false,
        headerShown: false,
      }}>
      <StackNavigatorRoot.Screen
        name="LoadingContainer"
        component={LoadingContainer}
      />
      <StackNavigatorRoot.Screen name="OnboardingNav" component={Onboarding} />
      <StackNavigatorRoot.Screen
        name="MainNavigation"
        component={SideMenuContainer}
        initialParams={{screenProps}}
      />
    </StackNavigatorRoot.Navigator>
  );
}

// Connecting Redux to Components in React-Navigation
const SideMenuContainer = connect(state => ({
  lang: state.settings.language, // TODO(cslim): Language selection for "Francais" is defaulted to en. To fix
  coach: state.settings.coach,
  unreadMessages: state.guistate.unreadMessages,
  connectionState: state.serverSyncStatus.connectionState,
}))(SideMenu);

export default function App(props) {
  return (
    <NavigationContainer>
      <RootStack screenProps={props.screenProps} />
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  navbarTitle: {
    color: 'black',
  },
  navbar: {
    backgroundColor: Colors.navigationBar.background,
  },
  actionButtonIcon: {
    fontSize: 26,
    height: 26,
    color: Colors.sideMenu.actionButton,
  },
  bottom: {
    flex: 0,
  },
  versionInfo: {
    alignItems: 'center',
    top: '45%',
  },
  coachAvater: {
    height: 80,
    width: 80,
    alignSelf: 'center',
    marginTop: 10,
  },
  coachName: {
    fontWeight: 'bold',
    fontSize: 20,
    paddingTop: 5,
    paddingBottom: 10,
    alignSelf: 'center',
  },
  drawerLabel: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  drawerLabelText: {
    fontSize: normalize(10),
  },
  badge: {
    backgroundColor: Colors.sideMenu.actionButton,
  },
  badgeText: {
    fontSize: normalize(9),
    paddingHorizontal: 10,
  },
});
