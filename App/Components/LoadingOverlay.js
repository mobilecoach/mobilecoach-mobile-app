import React, { Component } from 'react';
import Spinner from 'react-native-spinkit';

import BlurView from './BlurView';

export default class LoadingOverlay extends Component {
  static defaultProps = {
    type: 'Circle',
    color: '#fff',
    size: 45,
  };
  render() {
    return (
      <BlurView
        opacity={this.props.backgroundOpacity}
        fadeIn
        containerStyle={{
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Spinner {...this.props} />
      </BlurView>
    );
  }
}
